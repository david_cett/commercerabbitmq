﻿using ECommerceLibrary;
using ProgettoECommerce.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgettoECommerce.Controllers
{
    public class PrezzoController : Controller
    {

        private IPrezzoSqlProvider _prezzoSqlProvider;

        public PrezzoController()
        {

            _prezzoSqlProvider = new PrezzoSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);

        }
        
        public ActionResult Index()
        {
            var prezzi = _prezzoSqlProvider.GetAll().Select(n => new PrezzoView(n));
            return View(prezzi);
        }

        

       
        public ActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        public ActionResult Create(PrezzoView prezzo)
        {
            try
            {
                var b = _prezzoSqlProvider.GetAll().Any(p => p.Paese == prezzo.Paese && p.C8 == prezzo.C8);
                if (b == false)
                {
                    _prezzoSqlProvider.Insert(prezzo.ToPrezzo());
                }
                   
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [Route("Prezzo/Edit/{IdPrezzo}")]
        public ActionResult Edit(int IdPrezzo)
        {
            return View(new PrezzoView(_prezzoSqlProvider.Find(IdPrezzo)));
        }

        [Route("Prezzo/Edit/{IdPrezzo}")]
        [HttpPost]
        public ActionResult Edit(PrezzoView prezzo)
        {
            try
            {  
                    _prezzoSqlProvider.Edit(prezzo.ToPrezzo());

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
