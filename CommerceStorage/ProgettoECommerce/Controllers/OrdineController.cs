﻿using ECommerceLibrary;
using ProgettoECommerce.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgettoECommerce.Controllers
{
    public class OrdineController : Controller
    {

        private IOrdineSqlProvider _ordiniSqlProvider;

        public OrdineController()
        {

            _ordiniSqlProvider = new OrdiniSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);

        }
        
        public ActionResult Index()
        {
            var ordini = _ordiniSqlProvider.GetAll().Select(n => new OrdineView(n));
            return View(ordini);
        }
       
      
    }
}
