﻿using ECommerceLibrary;
using ProgettoECommerce.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgettoECommerce.Controllers
{
   
    public class ProdottoController : Controller
    {
        private IProdottoSqlProvider _prodottoSqlProvider;
        private IProducer _producer;

        public ProdottoController(IProducer producer)
        {
		
            _prodottoSqlProvider = new ProdottoSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _producer = producer;

    
        }
        
        public ActionResult Index()
        {
            var prodotti = _prodottoSqlProvider.GetAll().Select(n => new ProdottoView(n));
            return View(prodotti);
        }

        public JsonResult Cerca(string parametro)
        {
            var prodotti = _prodottoSqlProvider.Search(parametro);
            return Json(prodotti,JsonRequestBehavior.AllowGet);
        }

        public ActionResult CercaPage()
        {
            return View();
        }

       

       
        public ActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        public ActionResult Create(ProdottoView prodotto)
        {
            try
            {
                //var prodotti = _prodottoSqlProvider.GetAll();
                //if (prodotti == null)
                //{
                //    prodotto.C8 = "10000000";
                //    _prodottoSqlProvider.Insert(prodotto.ToProdotto());

                //}
                //else
                //{
                //    var c8max = _prodottoSqlProvider.GetMaxC8() + 1;
                //    prodotto.C8 = c8max.ToString();
                //    _prodottoSqlProvider.Insert(prodotto.ToProdotto());
                //}
                _producer.ProduceMessege(prodotto.ToProdotto());
                return RedirectToAction("Create");
            }
            catch
            {
                return View();
            }
        }

        [Route("Prodotto/Edit/{IdProdotto}")]
        public ActionResult Edit(int IdProdotto)
        {
            return View(new ProdottoView(_prodottoSqlProvider.Find(IdProdotto)));
        }

        [Route("Prodotto/Edit/{IdProdotto}")]
        [HttpPost]
        public ActionResult Edit(ProdottoView prodotto)
        {
            try
            {
              
                _prodottoSqlProvider.Edit(prodotto.ToProdotto());

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

       
       
    }
}
