﻿using ECommerceLibrary;
using ProgettoECommerce.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ProgettoECommerce.Controllers
{
    public class CatalogoController : Controller
    {
        private ICatalogoSqlProvider _catalogoSqlProvider;
        private IPrezzoSqlProvider _prezziSqlProvider;
        private IOrdineSqlProvider _ordineSqlProvider;
        private IClienteSqlProvider _clienteSqlProvider;
        private IProdottoSqlProvider _prodottoSqlProvider;


        public CatalogoController()
        {

            _catalogoSqlProvider = new CatalogoSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _prezziSqlProvider = new PrezzoSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _ordineSqlProvider = new OrdiniSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _clienteSqlProvider = new ClientiSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _prodottoSqlProvider = new ProdottoSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
        }

        public ActionResult ChangeCountry()
        {
           
            return View();
        }

       
        
        public ActionResult Index(string nazione)
        {
            var catalogo = _catalogoSqlProvider.GetCatalogo(nazione);
            var catalogoView =catalogo.Select(n => new CatalogoView(n)).ToList();
            return View(catalogoView);
        }  



        [HttpGet]
        public JsonResult GetPaesi()
        {
            var paesi = _catalogoSqlProvider.GetPeasi();
           
            
            return Json(paesi, JsonRequestBehavior.AllowGet);
        }


        [Route("Catalogo/Ordina/{c8}/{prezzoProdotto}/{valuta}/{idProdotto}")]

        public ActionResult Ordina(string c8, string prezzoProdotto, string valuta,string idProdotto)
        {
            var cliente = _clienteSqlProvider.GetAll().First();
            var prodotto = _prodottoSqlProvider.Find(Convert.ToInt32(idProdotto));
            if (prodotto.Quantita > 0)
            {
                if (cliente != null)
                {
                    var ordine = new Ordine()
                    {
                        Prezzo = Convert.ToDouble(prezzoProdotto),
                        DataAcquisto = DateTime.Now.Date.ToString("d"),
                        C8 = c8,
                        Valuta = valuta,
                        IdUtente = cliente.IdCliente

                    };
                    _ordineSqlProvider.Insert(ordine);


                    prodotto.Quantita--;
                    _prodottoSqlProvider.Edit(prodotto);


                }
            }

            return RedirectToAction("Index","Ordine");

        }

       
    }
}
