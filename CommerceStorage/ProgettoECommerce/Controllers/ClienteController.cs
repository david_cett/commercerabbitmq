﻿using ECommerceLibrary;
using ProgettoECommerce.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgettoECommerce.Controllers
{
    public class ClienteController : Controller
    {
        private IClienteSqlProvider _clienteSqlProvider;

        public ClienteController()
        {

            _clienteSqlProvider = new ClientiSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);

        }
       
        public ActionResult Index()
        {
            var clienti = _clienteSqlProvider.GetAll().Select(n => new ClienteView(n));
            return View(clienti);
        }

       

        
        public ActionResult Create()
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult Create(ClienteView cliente)
        {
            try
            {

                if (!_clienteSqlProvider.GetAll().Any(p => p.Email == cliente.Email))
                {
                    _clienteSqlProvider.Insert(cliente.ToCliente());
                }
                else
                {
                    _clienteSqlProvider.Edit(cliente.ToCliente());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [Route("Cliente/Edit/{IdCliente}")]
       
        public ActionResult Edit(int IdCliente)
        {
            return View(new ClienteView(_clienteSqlProvider.Find(IdCliente)));
        }
        [Route("Cliente/Edit/{IdCliente}")]
        
        [HttpPost]
        public ActionResult Edit(ClienteView cliente)
        {
            try
            {
                _clienteSqlProvider.Edit(cliente.ToCliente());

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

       
    }
}
