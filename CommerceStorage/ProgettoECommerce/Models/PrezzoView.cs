﻿using ECommerceLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProgettoECommerce.Models
{
    public class PrezzoView
    {
        [Display(Name ="Codice Prodotto")]
        public string C8 { get; set; }
        [Display(Name = "Prezzo Prodotto")]
        [RegularExpression(@"^\d+(.\d{1,2})?$",ErrorMessage ="Il prezzo deve esser in formato dd.dd")]
        public double PrezzoProdotto { get; set; }
        [RegularExpression(@"(\w\w\w)",ErrorMessage ="Formatto valuta xxx")]
        public string Valuta { get; set; }
        [RegularExpression(@"(\w\w)", ErrorMessage = "Formatto valuta xx")]
        public string Paese { get; set; }
        [Display(Name = "Id Prezzo")]
        public  int IdPrezzo { get; set; }

        public PrezzoView()
        {

        }

        public PrezzoView(Prezzo prezzo)
        {
            C8 = prezzo.C8;
            PrezzoProdotto = prezzo.PrezzoProdotto;
            Valuta = prezzo.Valuta;
            Paese = prezzo.Paese;
            IdPrezzo = prezzo.IdPrezzo;
        }

        public Prezzo ToPrezzo() => new Prezzo(this.IdPrezzo)
        {
            C8 = this.C8,
            Paese = this.Paese,
            PrezzoProdotto = this.PrezzoProdotto,
            Valuta = this.Valuta
        };
    }
}