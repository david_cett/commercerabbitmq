﻿using ECommerceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProgettoECommerce.Models
{
    public class CatalogoView
    {
        public string Titolo { get; set; }

        public string Descrizione { get; set; }

        public string UrlImmagine { get; set; }

        public double PrezzoProdotto { get; set; }

        public string Valuta { get; set; }

        public string Disponibile { get; set; }

        public string C8 { get; set; }

        public int Idprodotto { get; set; }

        public CatalogoView()
        {

        }

        public CatalogoView(Catalogo catalogo)
        {
            Titolo = catalogo.Titolo;
            Descrizione = catalogo.Descrizione;
            UrlImmagine = catalogo.UrlImmagine;
            PrezzoProdotto = catalogo.PrezzoProdotto;
            Valuta = catalogo.Valuta;
            C8 = catalogo.C8;
            Idprodotto = catalogo.IdProdotto;
            if (catalogo.Quantita == 0)
            {
                Disponibile = "Non disponibile";
            }
            else
            {
                Disponibile = "Disponibile";
            }
        }

    }
}