﻿using ECommerceLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProgettoECommerce.Models
{
    public class OrdineView
    {
        [Display(Name = "Id Ordine")]
        public  int IdOrdine { get; set; }
        [Display(Name ="Id Prodotto")]
        public int IdProdotto { get; set; }
        [Display(Name = "Id Utente")]
        public int IdUtente { get; set; }

        public double Prezzo { get; set; }

        public string Valuta { get; set; }
        [Display(Name = "Data Aquisto")]
        public string DataAcquisto { get; set; }

        public string C8 { get; set; }

        public OrdineView()
        {

        }

        public OrdineView(Ordine ordine)
        {
            IdOrdine =ordine. IdOrdine;
            //IdProdotto = ordine.IdProdotto;
            IdUtente = ordine.IdUtente;
            Prezzo = ordine.Prezzo;
            Valuta = ordine.Valuta;
            DataAcquisto = ordine.DataAcquisto;
            C8 = ordine.C8;
        }
    }
}