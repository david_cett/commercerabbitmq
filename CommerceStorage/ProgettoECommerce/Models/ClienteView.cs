﻿using ECommerceLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProgettoECommerce.Models
{
    public class ClienteView
    {
        [Display(Name ="Id Cliente")]
        public  int IdCliente { get; set; }
        [RegularExpression(@"(\w)+@(\w)+.(\w\w)(\w)?",ErrorMessage ="La mail non è valida")]
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }

        [Display(Name = "Data di Nascita")]
        [RegularExpression(@"(\d\d).(\d\d).(\d\d\d\d)", ErrorMessage ="La data deve essere in formato DD.MM.AAAA")]
        public string DataDiNascita { get; set; }

        public ClienteView()
        {

        }

        public ClienteView(Cliente cliente)
        {
            IdCliente = cliente.IdCliente;
            Email = cliente. Email;
            Nome = cliente. Nome;
            Cognome = cliente. Cognome;
            DataDiNascita = cliente.DataDiNascita;
               
        }

        public Cliente ToCliente() => new Cliente(this.IdCliente)
        {
            Email = this.Email,
            Cognome = this.Cognome,
            DataDiNascita = this.DataDiNascita,
            Nome = this.Nome
        };
    }
}