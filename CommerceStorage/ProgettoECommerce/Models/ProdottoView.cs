﻿using ECommerceLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProgettoECommerce.Models
{
    public class ProdottoView
    {
        public string Titolo { get; set; }

        public string Descrizione { get; set; }

        public string UrlImmagine { get; set; }

        [Display(Name = "Id Prodotto")]
        public int IdProdotto { get; set; }

        [Display(Name="Codice Prodotto")]
        public string C8 { get; set; }

        [Range(0,1000,ErrorMessage ="La quantita deve essere maggiore di 0 é minore di 1000")]
        public int Quantita { get; set; }

        public ProdottoView()
        {

        }

        public ProdottoView(Prodotto prodotto)
        {
            Titolo = prodotto.Titolo;
            Descrizione = prodotto. Descrizione;
            UrlImmagine = prodotto. UrlImmagine;
            IdProdotto = prodotto.IdProdotto;
            C8 = prodotto.C8;
            Quantita = prodotto.Quantita;
        }

        public Prodotto ToProdotto() => new Prodotto(this.IdProdotto) { C8 = this.C8, Descrizione = this.Descrizione, Titolo = this.Titolo, UrlImmagine = this.UrlImmagine,Quantita=this.Quantita };
    }
}