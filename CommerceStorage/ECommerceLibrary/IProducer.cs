﻿using System;

namespace ECommerceLibrary
{
    public interface IProducer
    {
        void ProduceMessege(Object obj);
    }
}
