﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public class Producer:IProducer
    {
        private readonly ConnectionFactory _factory;

        public Producer()
        {
            _factory= new ConnectionFactory() { HostName = "localhost" };
        }

        public void ProduceMessege(Object obj)
        {
            string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            using (var connection = _factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "prodotti",
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                string message = json;
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                routingKey: "prodotti",
                basicProperties: null,
                body: body);
            }
        }
    }
}
