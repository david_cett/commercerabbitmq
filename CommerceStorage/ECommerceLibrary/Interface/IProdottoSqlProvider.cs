﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public interface IProdottoSqlProvider
    {
        void Insert(Prodotto prodotto);
        Prodotto Find(int id);
        void Edit(Prodotto prodotto);
        IEnumerable<Prodotto> GetAll();

        int GetMaxC8();

        IEnumerable<Prodotto> Search(string ricerca);


    }


}
