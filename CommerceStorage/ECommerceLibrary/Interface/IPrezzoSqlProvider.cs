﻿using System.Collections.Generic;

namespace ECommerceLibrary
{
    public interface IPrezzoSqlProvider
    {
        void Insert(Prezzo prezzo);
        Prezzo Find(int id);
        void Edit(Prezzo prezzo);
        IEnumerable<Prezzo> GetAll();
    }


}
