﻿using System.Collections.Generic;

namespace ECommerceLibrary
{
    public interface IClienteSqlProvider
    {
        void Insert(Cliente cliente);
        Cliente Find(int id);
        void Edit(Cliente cliente);
        IEnumerable<Cliente> GetAll();
    }


}
