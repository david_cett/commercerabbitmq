﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public interface ICatalogoSqlProvider
    {
        IEnumerable<Catalogo> GetCatalogo(string paese);
        IEnumerable<Paese> GetPeasi();
    }
}
