﻿using System.Collections.Generic;

namespace ECommerceLibrary
{
    public interface IOrdineSqlProvider
    {
        void Insert(Ordine ordine);
        Ordine Find(int id);
        
        IEnumerable<Ordine> GetAll();
    }


}
