﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public class CatalogoSqlProvider : SqlProvider, ICatalogoSqlProvider
    {
        public CatalogoSqlProvider(string connectionString):base(connectionString)
        {

        }
        public IEnumerable<Catalogo> GetCatalogo(string paese)
        {
            List<Catalogo> catalogo = new List<Catalogo>();

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"Select Prodotti.C8,IdProdotto,Titolo,Descrizione,UrlImmagine,Quantita,Prezzo,Valuta
                                            from Prodotti
                                            join Prezzi on Prezzi.C8=Prodotti.C8
                                            where Paese=@Paese", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@Paese", paese);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                  

                    catalogo.Add(new Catalogo()
                        {
                            Titolo = reader["Titolo"].ToString(),
                            Descrizione = reader["Descrizione"].ToString(),
                            UrlImmagine = reader["UrlImmagine"].ToString(),
                            C8 = reader["C8"].ToString(),
                            Valuta = reader["Valuta"].ToString(),
                            PrezzoProdotto = Convert.ToDouble(reader["Prezzo"].ToString()),
                            Quantita= Convert.ToInt32(reader["Quantita"].ToString()),
                            IdProdotto = Convert.ToInt32(reader["IdProdotto"].ToString())
                    }); 
            }
            return catalogo;
        }

        public IEnumerable<Paese> GetPeasi()
        {
            List<Paese> paesi = new List<Paese>();
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"select Paese
                                                from Prezzi
                                                group by Paese", conn))
            {
                conn.Open();
                
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    paesi.Add(new Paese(reader["Paese"].ToString()));
                }
  
            }
            return paesi;
        }
    }
}
