﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public  class PrezzoSqlProvider : SqlProvider,IPrezzoSqlProvider
    {
        public PrezzoSqlProvider(string connectionString):base(connectionString)
        {

        }
        public  void Edit(Prezzo prezzo)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"UPDATE [dbo].[Prezzi]
                                              SET [C8] = @C8
                                              ,[Paese] = @Paese
                                              ,[Prezzo] = @Prezzo
                                              ,[Valuta] = @Valuta
                                              WHERE IdPrezzo=@IdPrezzo", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@C8", prezzo.C8);
                cmd.Parameters.AddWithValue("@Paese", prezzo.Paese);
                cmd.Parameters.AddWithValue("@Prezzo", prezzo.PrezzoProdotto);
                cmd.Parameters.AddWithValue("@Valuta", prezzo.Valuta);
                cmd.Parameters.AddWithValue("@IdPrezzo", prezzo.IdPrezzo);
                cmd.ExecuteNonQuery();
            }
        }



        public Prezzo Find(int id)
        {
            try
            {
                using (var conn = new SqlConnection(connectionString))
                using (var cmd = new SqlCommand(@"SELECT [IdPrezzo]
                                                  ,[C8]
                                                  ,[Paese]
                                                  ,[Prezzo]
                                                  ,[Valuta]
                                                 FROM [dbo].[Prezzi]
                                                 WHERE IdPrezzo = @IdPrezzo", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@IdPrezzo", id);
                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                        return new Prezzo(Convert.ToInt32(reader["IdPrezzo"].ToString()))
                        {
                            C8 = reader["C8"].ToString(),
                            Paese = reader["Paese"].ToString(),
                            PrezzoProdotto = Convert.ToDouble(reader["Prezzo"].ToString()),
                            Valuta = reader["Valuta"].ToString(),
                          

                        };
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }



        public IEnumerable<Prezzo> GetAll()
        {
            List<Prezzo> prezzi = new List<Prezzo>();

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdPrezzo]
                                              ,[C8]
                                              ,[Paese]
                                              ,[Prezzo]
                                              ,[Valuta]
                                              FROM [dbo].[Prezzi] ", conn))

            {
                conn.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    prezzi.Add(new Prezzo(Convert.ToInt32(reader["IdPrezzo"].ToString()))
                    {

                        C8 = reader["C8"].ToString(),
                        Paese = reader["Paese"].ToString(),
                        PrezzoProdotto = Convert.ToDouble(reader["Prezzo"].ToString()),
                        Valuta = reader["Valuta"].ToString(),

                    });
            }
            return prezzi;
        }



        public  void Insert(Prezzo prezzo)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"INSERT INTO [dbo].[Prezzi]
                                              ([C8]
                                              ,[Paese]
                                              ,[Prezzo]
                                              ,[Valuta])
                                              VALUES
                                              (@C8
                                              ,@Paese
                                              ,@Prezzo
                                              ,@Valuta)", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@C8", prezzo.C8);
                cmd.Parameters.AddWithValue("@Paese", prezzo.Paese);
                cmd.Parameters.AddWithValue("@Prezzo", prezzo.PrezzoProdotto);
                cmd.Parameters.AddWithValue("@Valuta", prezzo.Valuta);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
