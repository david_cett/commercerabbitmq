﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public class ClientiSqlProvider : SqlProvider,IClienteSqlProvider
    {

        public ClientiSqlProvider(string connectionString):base(connectionString)
        {

        }
        public  void Edit(Cliente cliente)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"UPDATE [dbo].[Clienti]
                                              SET [Email] = @Email
                                              ,[Nome] = @Nome
                                              ,[Cognome] = @Cognome
                                              ,[DataDiNascita] = @DataDiNascita
                                              WHERE IdCliente=@IdCliente", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@Email", cliente.Email);
                cmd.Parameters.AddWithValue("@Nome", cliente.Nome);
                cmd.Parameters.AddWithValue("@Cognome", cliente.Cognome);
                cmd.Parameters.AddWithValue("@DataDiNascita", cliente.DataDiNascita);
                cmd.Parameters.AddWithValue("@IdCliente", cliente.IdCliente);
                cmd.ExecuteNonQuery();
            }
        }



        public  Cliente Find(int id)
        {
            try
            {
                using (var conn = new SqlConnection(connectionString))
                using (var cmd = new SqlCommand(@"SELECT [IdCliente]
                                                  ,[Email]
                                                  ,[Nome]
                                                  ,[Cognome]
                                                  ,[DataDiNascita]
                                                  FROM [dbo].[Clienti]
                                                  WHERE IdCliente = @IdCliente", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("IdCliente", id);
                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                        return new Cliente(Convert.ToInt32(reader["IdCliente"].ToString()))
                        {
                            Email = reader["Email"].ToString(),
                            Nome = reader["Nome"].ToString(),
                            Cognome = reader["Cognome"].ToString(),
                            DataDiNascita = reader["DataDiNascita"].ToString()

                        };
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }



        public  IEnumerable<Cliente> GetAll()
        {
            List<Cliente> clienti = new List<Cliente>();

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdCliente]
                                              ,[Email]
                                              ,[Nome]
                                              ,[Cognome]
                                              ,[DataDiNascita]
                                              FROM [dbo].[Clienti]", conn))
            {
                conn.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    clienti.Add(new Cliente(Convert.ToInt32(reader["IdCliente"].ToString()))
                    {
                            Email = reader["Email"].ToString(),
                            Nome = reader["Nome"].ToString(),
                            Cognome = reader["Cognome"].ToString(),
                            DataDiNascita = reader["DataDiNascita"].ToString()

                    });
            }
            return clienti;
        }



        public  void Insert(Cliente cliente)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"INSERT INTO [dbo].[Clienti]
                                              ([Email]
                                              ,[Nome]
                                              ,[Cognome]
                                              ,[DataDiNascita])
                                              VALUES
                                              (@Email
                                              ,@Nome
                                              ,@Cognome
                                              ,@DataDiNascita)", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@Email", cliente.Email);
                cmd.Parameters.AddWithValue("@Nome", cliente.Nome);
                cmd.Parameters.AddWithValue("@Cognome", cliente.Cognome);
                cmd.Parameters.AddWithValue("@DataDiNascita", cliente.DataDiNascita);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
