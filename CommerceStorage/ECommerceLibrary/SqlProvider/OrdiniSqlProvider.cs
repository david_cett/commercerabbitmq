﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public class OrdiniSqlProvider : SqlProvider,IOrdineSqlProvider
    {
        public OrdiniSqlProvider(string connectionString):base(connectionString)
        {

        }
       


        public Ordine Find(int id)
        {
            try
            {
                using (var conn = new SqlConnection(connectionString))
                using (var cmd = new SqlCommand(@"SELECT [IdOrdine]
                                                 ,[IdUtente]
                                                 ,[IdProdotto]
                                                 ,[Prezzo]
                                                 ,[Valuta]
                                                 ,[DataAcquisto]
                                                 FROM [dbo].[Ordini]
                                                 WHERE IdOrdine = @IdOrdine", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@IdProdotto", id);
                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                        return new Ordine()
                        {
                            IdUtente = Convert.ToInt32(reader["IdUtente"].ToString()),
                            C8 = reader["IdProdotto"].ToString(),
                            Prezzo = Convert.ToDouble(reader["IdProdotto"].ToString()),
                            Valuta = reader["Valuta"].ToString(),
                            DataAcquisto =reader["DataAcquisto"].ToString()

                        };
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }



        public IEnumerable<Ordine> GetAll()
        {
            List<Ordine> ordini = new List<Ordine>();

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdOrdine]
                                                 ,[IdUtente]
                                                 ,[C8]
                                                 ,[Prezzo]
                                                 ,[Valuta]
                                                 ,[DataAcquisto]
                                                 FROM [dbo].[Ordini]", conn))
            {
                conn.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    ordini.Add(new Ordine()
                    {
                        IdOrdine= Convert.ToInt32(reader["IdOrdine"].ToString()),
                        IdUtente = Convert.ToInt32(reader["IdUtente"].ToString()),
                        C8 = reader["C8"].ToString(),
                        Prezzo = Convert.ToDouble(reader["Prezzo"].ToString()),
                        Valuta = reader["Valuta"].ToString(),
                        DataAcquisto = reader["DataAcquisto"].ToString()

                    });
            }
            return ordini;
        }



        public  void Insert(Ordine ordine)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"INSERT INTO [dbo].[Ordini]
                                              ([IdUtente]                                          
                                              ,[Prezzo]
                                              ,[Valuta]
                                              ,[DataAcquisto]
                                              ,[C8])
                                              VALUES
                                              (@IdUtente                                             
                                              ,@Prezzo
                                              ,@Valuta
                                              ,@DataAcquisto
                                              ,@C8)", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@IdUtente", ordine.IdUtente);
                cmd.Parameters.AddWithValue("@Prezzo", ordine.Prezzo);
                cmd.Parameters.AddWithValue("@Valuta", ordine.Valuta);
                cmd.Parameters.AddWithValue("@DataAcquisto", ordine.DataAcquisto);
                cmd.Parameters.AddWithValue("@C8", ordine.C8);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
