﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ECommerceLibrary
{
    public class ProdottoSqlProvider : SqlProvider,IProdottoSqlProvider
    {
        public ProdottoSqlProvider(string connectionString):base(connectionString)
        {

        }
      

        public void Edit(Prodotto prodotto)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"UPDATE [dbo].[Prodotti]
                                              SET [C8] = @C8
                                              ,[Descrizione] = @Descrizione
                                              ,[Titolo] = @Titolo
                                              ,[UrlImmagine] = @UrlImmagine
                                              ,[Quantita]=@Quantita
                                              WHERE IdProdotto=@IdProdotto", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@C8", prodotto.C8);
                cmd.Parameters.AddWithValue("@Descrizione", prodotto.Descrizione);
                cmd.Parameters.AddWithValue("@Titolo", prodotto.Titolo);
                cmd.Parameters.AddWithValue("@UrlImmagine", prodotto.UrlImmagine);
                cmd.Parameters.AddWithValue("@Quantita", prodotto.Quantita);
                cmd.Parameters.AddWithValue("@IdProdotto", prodotto.IdProdotto);
                cmd.ExecuteNonQuery();
            }
        }

        

       

        public  Prodotto Find(int id)
        {
            
            try
            {
                using (var conn = new SqlConnection(connectionString))
                using (var cmd = new SqlCommand(@"SELECT [IdProdotto]
                                                  ,[C8]
                                                  ,[Descrizione]
                                                  ,[Titolo]
                                                  ,[UrlImmagine]
                                                  ,[Quantita]
                                                  FROM [dbo].[Prodotti]
                                                  WHERE IdProdotto = @IdProdotto", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@IdProdotto", id);
                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                        return new Prodotto(Convert.ToInt32(reader["IdProdotto"].ToString()))
                        {
                            Titolo = reader["Titolo"].ToString(),
                            Descrizione = reader["Descrizione"].ToString(),
                            UrlImmagine = reader["UrlImmagine"].ToString(),
                            C8 = reader["C8"].ToString(),
                            Quantita=Convert.ToInt32(reader["Quantita"].ToString())

                        };
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }



        public  IEnumerable<Prodotto> GetAll()
        {
            List<Prodotto> prodotti = new List<Prodotto>();

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdProdotto]
                                            ,[C8]
                                            ,[Descrizione]
                                            ,[Titolo]
                                            ,[UrlImmagine]
                                            ,[Quantita]
                                            FROM [dbo].[Prodotti]", conn))
            {
                conn.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    prodotti.Add(new Prodotto(Convert.ToInt32(reader["IdProdotto"].ToString()))
                    {
                        Titolo = reader["Titolo"].ToString(),
                        Descrizione = reader["Descrizione"].ToString(),
                        UrlImmagine = reader["UrlImmagine"].ToString(),
                        C8 = reader["C8"].ToString(),
                        Quantita =Convert.ToInt32(reader["Quantita"])
                    });
            }
            return prodotti;
        }

        public int GetMaxC8()
        {
           
                using (var conn = new SqlConnection(connectionString))
                using (var cmd = new SqlCommand(@"select max(cast(p.C8 as int)) as Max
                                                  from Prodotti p", conn))
                {
                    conn.Open();
                  
                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        return Convert.ToInt32(reader["Max"].ToString());
                    }
                return 99999998;
     
                       
                }
               
           
        }

        public void Insert(Prodotto prodotto)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"INSERT INTO [dbo].[Prodotti]
                                           ([C8]
                                           ,[Descrizione]
                                           ,[Titolo]
                                           ,[UrlImmagine]
                                           ,[Quantita])
                                            VALUES
                                           (@C8
                                           ,@Descrizione
                                           ,@Titolo
                                           ,@UrlImmagine
                                           ,@Quantita)", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@C8", prodotto.C8);
                cmd.Parameters.AddWithValue("@Descrizione", prodotto.Descrizione);
                cmd.Parameters.AddWithValue("@Titolo", prodotto.Titolo);
                cmd.Parameters.AddWithValue("@UrlImmagine", prodotto.UrlImmagine);
                cmd.Parameters.AddWithValue("@Quantita", prodotto.Quantita);
                cmd.ExecuteNonQuery();
            }
        }

        public IEnumerable<Prodotto> Search(string ricerca)
        {
            List<Prodotto> prodotti = new List<Prodotto>();

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdProdotto]
                                            ,[C8]
                                            ,[Descrizione]
                                            ,[Titolo]
                                            ,[UrlImmagine]
                                            ,[Quantita]
                                            FROM [dbo].[Prodotti]
                                            WHERE lower(Titolo) like '%" + ricerca + "%' or C8 like'%" + ricerca + "%'", conn))
            {
                conn.Open();
               
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    prodotti.Add(new Prodotto(Convert.ToInt32(reader["IdProdotto"].ToString()))
                    {
                        Titolo = reader["Titolo"].ToString(),
                        Descrizione = reader["Descrizione"].ToString(),
                        UrlImmagine = reader["UrlImmagine"].ToString(),
                        C8 = reader["C8"].ToString(),
                        Quantita = Convert.ToInt32(reader["Quantita"])
                    });
            }
            return prodotti;
        }
    }


}
