﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public class Paese
    {
        public string Nome { get; set; }

        public Paese(string nome)
        {
            this.Nome = nome;
        }
    }
}
