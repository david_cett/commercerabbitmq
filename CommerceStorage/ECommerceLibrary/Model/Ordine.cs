﻿using System;

namespace ECommerceLibrary
{
    public class Ordine
    {
        public int IdOrdine { get; set; }

        public string C8 { get; set; }

        public int IdUtente { get; set; }

        public double Prezzo { get; set; }

        public string Valuta { get; set; }

        public string DataAcquisto { get; set; }

        public Ordine()
        {
            
        }


    }
}
