﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public class Catalogo
    {
        public string Titolo { get; set; }

        public string Descrizione { get; set; }

        public string UrlImmagine { get; set; }

        public double PrezzoProdotto { get; set; }

        public string Valuta { get; set; }

        public int Quantita { get; set; }

        public string Disponibile { get; set; }

        public string C8 { get; set; }

        public int IdProdotto { get; set; }
    }
}
