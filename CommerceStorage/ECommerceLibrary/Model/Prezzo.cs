﻿namespace ECommerceLibrary
{
    public class Prezzo
    {
        public string C8 { get; set; }
        public double PrezzoProdotto { get; set; }
        public string Valuta { get; set; }
        public string Paese { get; set; }

        public readonly int IdPrezzo;
        public Prezzo(int idPrezzo)
        {
            this.IdPrezzo = idPrezzo;
        }
    }
}
