﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerceLibrary
{
    public class Prodotto
    {
        public string Titolo { get; set; }

        public string Descrizione { get; set; }

        public string UrlImmagine { get; set; }

        public readonly int IdProdotto;

        public string C8 { get; set; }

        public int Quantita { get; set; }

        public Prodotto(int IdProdotto)
        {
            this.IdProdotto = IdProdotto;
            
        }



    }
}
