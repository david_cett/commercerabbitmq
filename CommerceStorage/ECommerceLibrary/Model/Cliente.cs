﻿using System;

namespace ECommerceLibrary
{
    public class Cliente
    {
        public readonly int IdCliente;
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }

        public string DataDiNascita { get; set; }

        public Cliente(int IdCliente)
        {
            this.IdCliente = IdCliente;
        }
    }
}
