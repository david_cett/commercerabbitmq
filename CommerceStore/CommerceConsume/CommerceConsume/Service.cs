﻿using ECommerceLibrary;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Configuration;
using System.Text;

namespace CommerceConsume
{
    public class Service
    {

        private IModel _channel;
        private IConnection _connection;
        private readonly IProdottoSqlProvider _prodottoSqlProvider;

        public Service()
        {
            _prodottoSqlProvider = new ProdottoSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);


        }

        public void Start()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            
                _channel.QueueDeclare(queue: "prodotti",
                    durable: false,
                     exclusive: false,
                     autoDelete: false,
                     arguments: null);

                var consumer = new EventingBasicConsumer(_channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    var prodotto = JsonConvert.DeserializeObject<Prodotto>(message);

                    var prodotti = _prodottoSqlProvider.GetAll();
                    if (prodotti == null)
                    {
                        prodotto.C8 = "10000000";
                        _prodottoSqlProvider.Insert(prodotto);

                    }
                    else
                    {
                        var c8max = _prodottoSqlProvider.GetMaxC8() + 1;
                        prodotto.C8 = c8max.ToString();
                        _prodottoSqlProvider.Insert(prodotto);
                    }
                   

                    Console.WriteLine(" [x] Received {0}", message);
                };
                _channel.BasicConsume(queue: "prodotti",
                                     autoAck: true,
                                     consumer: consumer);
              
            
        }

        public void Stop()
        {
            _connection.Close();
            _channel.Close();
        }

    }
}
