﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace CommerceConsume
{
    class Program
    {
        static void Main()
        {
            HostFactory.Run(c =>
            {

                c.SetServiceName("commerce-consumer");
                c.SetDisplayName("commerce-consumer");
                c.SetDescription("commerce-consumer");

              

                c.Service<Service>(s =>
                {
                    s.ConstructUsing(service => new Service());
                    s.WhenStarted(service => service.Start());                   
                    s.WhenStopped(service => service.Stop());
                });

                c.StartAutomatically();
                c.RunAsLocalService();
                c.EnableShutdown();

            });
        }
    }
}
